/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */     
#include "../LIBS/WS2813.h"

#include "dfsdm.h"
#include "usart.h"
#include <string.h>
#include <stdio.h>

#include <inttypes.h>

//#define KARR
/* USER CODE END Includes */

/* Variables -----------------------------------------------------------------*/
osThreadId defaultTaskHandle;

/* USER CODE BEGIN Variables */

#define AUDIO_BUFFER_LEN 256
static int32_t __audioBuffer[AUDIO_BUFFER_LEN];
TaskHandle_t micTask;
QueueHandle_t micData;
static char usarBuffer[24];
/* USER CODE END Variables */

/* Function prototypes -------------------------------------------------------*/
void StartDefaultTask(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* USER CODE BEGIN FunctionPrototypes */
void vMicTask(void* param);
/* USER CODE END FunctionPrototypes */

/* Hook prototypes */

/* Init FreeRTOS */

void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	ws2813_init_tim(&htim3, TIM_CHANNEL_1, 8);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */

  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
	xTaskCreate(vMicTask,
			"MicTask",
			500,
			NULL,
			2,
			&micTask);
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
	micData = xQueueCreate(10, sizeof(int32_t));
  /* USER CODE END RTOS_QUEUES */
}

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN StartDefaultTask */
	/* Infinite loop */
	for(;;)
	{
		osDelay(1);
	}
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Application */


void vMicTask(void* param){
	//Initialiser le Micro
	HAL_DFSDM_FilterRegularStart_DMA(&hdfsdm1_filter0, __audioBuffer, AUDIO_BUFFER_LEN);

	while(1){
		int32_t moyenne;

		if(xQueueReceive(micData, &moyenne, pdMS_TO_TICKS(10)) == pdTRUE){

			WS2813_COLOR color_on={.rgb={.r=0,.g=0,.b=0}};
			for(int i=0; i<8; i++){
				ws2813_update_led(i, color_on); //initialization des LED
			}

			const int maxValue = 200000;
			const int nbLeds = 9;
			int ech = maxValue / nbLeds;

			int current_led = moyenne / ech;
			if (current_led > 8)
				current_led = 8;
			if (current_led < 0)
				current_led = 0;
			//current_led = 7- current_led;

			int pct = (moyenne*100)/maxValue;
			if(pct > 100)
				pct = 100;
			if(pct < 0)
				pct = 0;
			snprintf(usarBuffer, 24, "%d%%\n",pct);
			HAL_UART_Transmit(&huart2, usarBuffer, strlen(usarBuffer), 100);


			for (int i = 1; i <= current_led; i++) {
				if(i > 6){//affichage ROUGE en bordure
					color_on.rgb.r=30;
				}else if(i > 2){//affichage JAUNE en bordure
					color_on.rgb.r=30;
					color_on.rgb.g=30;
				}else{//affichage VERT par défaut
					color_on.rgb.g=30;
				}
				if(i > 0)
					ws2813_update_led(i-1, color_on);
				color_on.rgb.r = 0;
				color_on.rgb.g = 0;
				color_on.rgb.b = 0;

			}
			// Transmet la trame aux LEDS
			ws2813_start_transmission_tim();
		}
	}


}

void HAL_DFSDM_FilterRegConvCpltCallback(DFSDM_Filter_HandleTypeDef *hdfsdm_filter){
	//call every 128 sec
	int index;
	int32_t moyenne = 0;
	//int64_t somme = 0;

	for (index=0; index<AUDIO_BUFFER_LEN; index++){
		//somme += __audioBuffer[index];
		//moyenne = max(moyenne, __audioBuffer[index]);
		if (__audioBuffer[index] > moyenne)
			moyenne = __audioBuffer[index];
		else
			moyenne = moyenne * 0.9;
	}
	//moyenne = somme/AUDIO_BUFFER_LEN;
	xQueueSendFromISR(micData, &moyenne, NULL);

}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
